# IndustrialCraft 2 Advanced Power Management #

This mod adds several new blocks to Industrial Craft 2 to help you manage your energy grid. We present for your crafting enjoyment the following high quality devices: The Charging Bench, the Battery Station, the Adjustable Transformer, and the Storage Monitor (plus the Emitter for creative builds).

## Credits ##

Pantheis: Author

Tallinu: Author

Drashian:

Starlight:

Zuxelus : Coder


### Forum ###

http://forum.industrial-craft.net/index.php?page=Thread&threadID=7900

### Downloads ##

Version APM-IC2-Ex-1.2.1-IC2-2.0.238-experimental.jar for IC2 2.0.238-experimental (Minecraft 1.6.4)

[APM-IC2-Ex-1.2.1-IC2-2.0.238-experimental.jar](http://forum.industrial-craft.net/index.php?page=Attachment&attachmentID=3431&h=36517f20732dcd147ae8bc3f9145ffc8f699e3be)

Version ic2apm-1.7.2-1.7.2.01.jar for IC2 2.1.484-experimental (Minecraft 1.7.2)

[ic2apm-1.7.2-1.7.2.01.jar](https://bitbucket.org/Zuxelus/ic2-advanced-power-management/downloads/ic2apm-1.7.2-1.7.2.01.jar)